# Load the OpenSSL library
require "openssl"

# Generate a new RSA key pair
key_pair = OpenSSL::PKey::RSA.new(2048)

# Extract the private key and public key
private_key = key_pair.to_pem
public_key = key_pair.public_key.to_pem

# Save the keys to files
File.write("private_key.pem", private_key)
File.write("public_key.pem", public_key)
