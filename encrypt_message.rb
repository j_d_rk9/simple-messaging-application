# Load the OpenSSL library
require "openssl"

# Read in the public key and the message to be encrypted
public_key = OpenSSL::PKey::RSA.new(File.read("public_key.pem"))
message = "This is a secret message"

# Use the public key to encrypt the message
encrypted_message = public_key.public_encrypt(message)

# Save the encrypted message to a file
File.write("encrypted_message.bin", encrypted_message)
