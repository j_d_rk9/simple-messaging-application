Rails.application.routes.draw do
    # Route for the encryption form
    get '/encrypt', to: 'messages#encrypt'
  
    # Route for the decryption view
    get '/decrypt', to: 'messages#decrypt'
  end
  