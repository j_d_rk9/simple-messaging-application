class MessagesController < ApplicationController
    # Load the OpenSSL library
    require "openssl"
  
    # Action for encrypting a message
    def encrypt
      # Read in the public key and the message to be encrypted
      public_key = OpenSSL::PKey::RSA.new(File.read("public_key.pem"))
      message = params[:message]
  
      # Use the public key to encrypt the message
      encrypted_message = public_key.public_encrypt(message)
  
      # Save the encrypted message to a file
      File.write("encrypted_message.bin", encrypted_message)
  
      # Redirect to the decryption form
      redirect_to decrypt_path
    end
  
    # Action for decrypting a message
    def decrypt
      # Read in the private key and the encrypted message
      private_key = OpenSSL::PKey::RSA.new(File.read("private_key.pem"))
      encrypted_message = File.read("encrypted_message.bin")
  
      # Use the private key to decrypt the message
      decrypted_message = private_key.private_decrypt(encrypted_message)
  
      # Render the decrypted message
      render plain: decrypted_message
    end
  end
  