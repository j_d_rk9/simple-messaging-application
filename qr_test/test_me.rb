require 'openssl'
require 'zlib'
require 'rqrcode'
require 'securerandom'


# Read the text from the file
text = File.read("text_file.txt")

# Generate a new RSA key pair
key_pair = OpenSSL::PKey::RSA.new(2048)

# Extract the private key and public key
private_key = key_pair.to_pem
public_key = key_pair.public_key.to_pem

# Save the keys to files
File.write("private_key.pem", private_key)
File.write("public_key.pem", public_key)

# Get the public key
public_key = OpenSSL::PKey::RSA.new(File.read("public_key.pem"))

# Create a new Cipher object for encryption
cipher = OpenSSL::Cipher::Cipher.new('AES-256-ECB')

# Initialize the Cipher object for encryption and set the key
cipher.encrypt
cipher.key = SecureRandom.random_bytes(32)

# Encrypt the text
encrypted_text = cipher.update(text) + cipher.final

# Compress the encrypted text
compressed_text = Zlib::Deflate.deflate(encrypted_text)

# Check the size of the compressed text
if compressed_text.bytesize > 2953
  puts "Error: Text is too large to fit in a QR code"
else
  # Generate a QR code for the compressed text
  qr = RQRCode::QRCode.new(compressed_text)
  # Save the QR code as a PNG image
  png = qr.as_png(resize_gte_to: false, resize_exactly_to: false, fill: 'white', color: 'black', size: 240)
  File.write('qr.png', png)
end 

  # Save
