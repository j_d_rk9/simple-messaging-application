require 'openssl'
require 'rqrcode'

# Read the QR code image file
png = File.read("qr0.png")

# Extract the QR code data from the image file
qr = RQRCode::QRCode.new(png)
encrypted_text = qr.data

# Get the private key
private_key = OpenSSL::PKey::RSA.new(File.read("private_key.pem"))

# Decrypt the encrypted text using the private key
decrypted_text = private_key.private_decrypt(encrypted_text)

# Write the decrypted text to a file
File.write("decrypted_text.txt", decrypted_text)
