require 'openssl'
require 'rqrcode'

# Read the text from the file
text = File.read("text_file.txt")

# Generate a new RSA key pair
key_pair = OpenSSL::PKey::RSA.new(2048)

# Extract the private key and public key
private_key = key_pair.to_pem
public_key = key_pair.public_key.to_pem

# Save the keys to files
File.write("private_key.pem", private_key)
File.write("public_key.pem", public_key)

# Get the public key
public_key = OpenSSL::PKey::RSA.new(File.read("public_key.pem"))

# Encrypt the text using the public key
encrypted_text = public_key.public_encrypt(text)

# Divide the encrypted text into chunks of size 2953 bytes
chunks = encrypted_text.chars.each_slice(2953).map(&:join)

# Generate a QR code for each chunk
chunks.each_with_index do |chunk, index|
  # Generate a QR code for the chunk
  qr = RQRCode::QRCode.new(chunk)
  # Save the QR code as a PNG image
  png = qr.as_png(resize_gte_to: false, resize_exactly_to: false, fill: 'white', color: 'black', size: 240)
  File.write("qr#{index}.png", png)
end
