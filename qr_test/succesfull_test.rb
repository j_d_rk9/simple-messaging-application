require 'openssl'
require 'zlib'
require 'rqrcode'
require 'securerandom'

# Read the QR code image from the file
qr_code = RQRCode::QRCode.new(File.read('qr.png'))

# Extract the compressed text from the QR code
compressed_text = qr_code.data

# Uncompress the text
encrypted_text = Zlib::Inflate.inflate(compressed_text)

# Read the private key from the file
private_key = OpenSSL::PKey::RSA.new(File.read("private_key.pem"))

# Create a new Cipher object for decryption
cipher = OpenSSL::Cipher::Cipher.new('AES-256-ECB')

# Initialize the Cipher object for decryption and set the key
cipher.decrypt
cipher.key = SecureRandom.random_bytes(32)

# Decrypt the text
text = cipher.update(encrypted_text) + cipher.final

# Print the decrypted text
puts text
