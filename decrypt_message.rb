# Load the OpenSSL library
require "openssl"

# Read in the private key and the encrypted message
private_key = OpenSSL::PKey::RSA.new(File.read("private_key.pem"))
encrypted_message = File.read("encrypted_message.bin")

# Use the private key to decrypt the message
decrypted_message = private_key.private_decrypt(encrypted_message)

# Print the decrypted message
puts decrypted_message
